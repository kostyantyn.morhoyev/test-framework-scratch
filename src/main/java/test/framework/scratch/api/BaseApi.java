package test.framework.scratch.api;

import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import test.framework.scratch.config.properties.Properties;

public class BaseApi {

    @Autowired
    Properties properties;

    //do not use for tests
    //request params will be stored and cause conflicts
    public static RequestSpecification defaultRequestSpecification
            = RestAssured.given()
            .config(
                    RestAssured.config()
                            .sslConfig(new SSLConfig().relaxedHTTPSValidation().allowAllHostnames())
            )
            .urlEncodingEnabled(true)
            .contentType(ContentType.JSON)
            .config(
                    RestAssured.config()
                            .sslConfig(new SSLConfig().relaxedHTTPSValidation().allowAllHostnames())
            );



    public RequestSpecification httpClient() {
        return RestAssured
                .given()
                .urlEncodingEnabled(false)
                .baseUri(properties.getBaseUrl())
                .contentType(properties.getContentType())
                .config(
                        RestAssured.config()
                                .sslConfig(new SSLConfig().relaxedHTTPSValidation().allowAllHostnames())
                )

                ;
    }

}
