package test.framework.scratch.api;

import io.restassured.response.Response;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import static test.framework.scratch.util.RequestUtil.slash;

@Component
public class SomeTestApi extends BaseApi {


    public Response getImage(Double psi, Integer id){
        return httpClient()
                .contentType("image/png")
                .get(properties.getApi().getResults() + slash("psi-" + psi) + slash("seed" + id + ".png"));
    }


}
