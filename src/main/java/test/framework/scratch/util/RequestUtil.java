package test.framework.scratch.util;

public class RequestUtil {

    private static final String slash = "/";


    public static String slash(Object somethingAfterSlash) {
        return slash + somethingAfterSlash;
    }

    protected static String requestBody(Object someObject) {
        if (someObject.getClass() == String.class) {
            return someObject.toString();
        } else {
            return JsonUtil.mapToJson(someObject);
        }
    }

    protected static String requestBody(Object someObject, Boolean serializeNulls) {
        if (someObject.getClass() == String.class) {
            return someObject.toString();
        } else {
            return JsonUtil.mapToJson(someObject, serializeNulls);
        }
    }

    protected static String requestBodyWithDecimals(Object someObject) {
        if (someObject.getClass() == String.class) {
            return someObject.toString();
        } else {
            return JsonUtil.calculationSerializer().toJson(someObject);
        }
    }
}
