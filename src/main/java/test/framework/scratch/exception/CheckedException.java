package test.framework.scratch.exception;

public class CheckedException extends RuntimeException{

    public CheckedException(){
        super();
    }
    public CheckedException(String message) {
        super(message);
    }
    public CheckedException(Object message) {
        super(message.toString());
    }
    public CheckedException(String message, Throwable cause) {
        super(message, cause);
    }
    public CheckedException(Throwable cause) {
        super(cause);
    }
    public CheckedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
