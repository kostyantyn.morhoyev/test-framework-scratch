package test.framework.scratch.dataprovider;

import org.testng.annotations.DataProvider;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class RandomImageDataProvider {

    List<Double> creativityList = List.of(0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0);
    List<Integer> imgSeed = IntStream.rangeClosed(10000, 100000).boxed().collect(Collectors.toList());

    private static  <T> T getRandomFromList(List<T> list) {
        int element =  ThreadLocalRandom.current().nextInt(list.size());
        return list.get(element);
    }

    @DataProvider(name = "random-image", parallel = false)
    public Object[][] getRandomImage(){
        return  // where 5 is amount of test runs
                IntStream.rangeClosed(0, 5).boxed()
                .map(i -> new Object[]{
                        getRandomFromList(creativityList),
                        getRandomFromList(imgSeed)
                })
                .toArray(Object[][]::new);
    }
}
