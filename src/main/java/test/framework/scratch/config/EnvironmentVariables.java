package test.framework.scratch.config;

import lombok.extern.log4j.Log4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j
public enum EnvironmentVariables {
    include, exclude, packages, classes, parallel, thread_count, data_provider_thread_count,
    application,
    // user
    // baseurl
    // anything
    ;
    // enum values represent any environment variables
    // if variable have same name as enum value, it will be stored as key-value
    // variables from jenkins, system.setproperty, maven params, everything are
    // being processed
    // used to configure test execution from external environments
    public static Map<EnvironmentVariables, String> envVars = new HashMap<>();

    public static String get(EnvironmentVariables value) {
        return envVars.get(value);
    }

    public static boolean isPresent(EnvironmentVariables value) {
        return envVars.get(value) != null;
    }

    public static Map<EnvironmentVariables, String> setVariables(Map<String, String> globalEnvVars) {
        Set<String> projectVars = Arrays.stream(EnvironmentVariables.values()).map(Enum::name)
                .collect(Collectors.toSet());
        log.info(projectVars.toString());

        // environment
        globalEnvVars.keySet().stream().filter(projectVars::contains)
                .forEach(var -> envVars.put(EnvironmentVariables.valueOf(var), globalEnvVars.get(var)));

        // properties
        System.getProperties().entrySet().stream().filter(pair -> projectVars.contains(pair.getKey())).forEach(
                pair -> envVars.put(EnvironmentVariables.valueOf((String) pair.getKey()), (String) pair.getValue()));
        log.info("Detected environment variables: " + envVars.size() + " \n" + envVars.toString());
        return envVars;
    }
}