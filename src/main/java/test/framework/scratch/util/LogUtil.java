package test.framework.scratch.util;

import com.google.gson.*;
import io.qameta.allure.Allure;
import lombok.extern.log4j.Log4j;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static test.framework.scratch.util.FuncUtil.optional;
import static test.framework.scratch.util.FuncUtil.silentOptional;

@Log4j
public class LogUtil {

    public static String format(Object object) {
        Object ref = object;
        if (object == null) return "null";
        if (object.getClass().equals(String.class)) {
            try {
                ref = new JsonParser().parse(object.toString());
            } catch (JsonSyntaxException ex) {
                return object.toString();
            }
        }
        return beautifyJsonString(JsonUtil.mapToJson(ref, JsonUtil.calculationSerializer()));
    }

    public static LogEntry logEntry(Object object) {
        StringBuilder sb = new StringBuilder();
        sb.append(object.getClass().getSimpleName() + "\n");
        fieldValueMap(object).forEach((k, v) -> {
            sb.append(k + ": " + v + "\n");
        });
        return new LogEntry(sb);
    }

    public LogEntry message(String descriptionErr) {
        return new LogEntry(descriptionErr);
    }

    public static String withLogs(Object entry) {
        if (entry.getClass() == String.class)
            return new LogEntry(String.class.cast(entry)).logToConsole().logToAllure("error").get();
        else
            return logEntry(entry).logToConsole().logToAllure("error").get();
    }

    protected static Map<String, String> fieldValueMap(Object object) {
        HashMap<String, String> fields = new HashMap<>();
        Arrays.stream(object.getClass().getDeclaredFields()).forEach(field -> {
            field.setAccessible(true);
            String value = silentOptional(() -> field.get(object).toString()).orElse("null");
            fields.put(field.getName(), value);
        });
        return fields;
    }

    public static class LogEntry {
        StringBuilder sb;

        public LogEntry(StringBuilder sb) {
            this.sb = sb;
        }

        public LogEntry(String str) {
            this.sb = new StringBuilder(str);
        }

        public String get() {
            return sb.toString();
        }

        public LogEntry logToConsole() {
            log.info(sb.toString());
            return this;
        }

        public LogEntry logToAllure(String name) {
            Allure.addAttachment(name, sb.toString());
            return this;
        }
    }

    private static final String emptyString = "";

    public static String getOrReplaceWith(String string, String nullReplacement) {
        return Optional.ofNullable(string).orElse(nullReplacement);
    }

    public static String toStringOrReplaceWith(Object object, String nullReplacement) {
        return Optional.ofNullable(object.toString()).orElse(nullReplacement);
    }

    public static String beautifyBody(String contentType, String body) {
        if (contentType.contains("json")) {
            return beautifyJsonString(body);
        } else {
            return getOrReplaceWith(body, emptyString);
        }
    }

    public static String beautifyJsonString(String json) {
        Gson prettySerializer = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
        JsonParser parser = new JsonParser();
        JsonElement beautifiedAsJson = optional(() -> parser.parse(json)).orElseGet(() -> {
                    System.out.println("Failed to beautify:" + json);
                    return new JsonObject();
                }
        );
        return getDefaultEncodingOf(prettySerializer.toJson(beautifiedAsJson));
    }

    //charset UTF8 does not support some symbols and display them on /u stile.
    public static String getDefaultEncodingOf(String encodedString) {
        return new String(encodedString.getBytes(StandardCharsets.UTF_8));
    }

    public static String getEncodingOf(String encodedString, Charset charset) {
        return new String(encodedString.getBytes(charset));
    }


    public static String beautifyObjectAsJsonString(Object object) {
        return beautifyJsonString(JsonUtil.mapToJson(object));
    }

    public static String beautifyObjectAsJsonString(Object object, Gson serializer) {
        return beautifyJsonString(JsonUtil.mapToJson(object, serializer));
    }


    public static String causedBy(String string) {
        return "Asserting values of: " + string + " result: ";
    }
}
