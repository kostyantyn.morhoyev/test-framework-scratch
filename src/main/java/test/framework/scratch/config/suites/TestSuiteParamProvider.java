package test.framework.scratch.config.suites;

import org.testng.IAlterSuiteListener;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static test.framework.scratch.config.EnvironmentVariables.*;

public class TestSuiteParamProvider implements IAlterSuiteListener {

    //to alter existing test suite with provided configs, such as parallel, threadcount, etc

    @Override
    public void alter(List<XmlSuite> suites) {
        XmlSuite xmlSuite;

        if (!suites.isEmpty()) {
            xmlSuite = suites.get(0);
            passParamsFromEnv(xmlSuite);
        }
    }

    private void passParamsFromEnv(XmlSuite xmlSuite) {
        setVariables(System.getenv());

        if (isPresent(parallel)) {
            XmlSuite.ParallelMode mode = XmlSuite.ParallelMode.getValidParallel(get(parallel).toUpperCase());
            xmlSuite.setParallel(mode);
        }

        if (isPresent(thread_count)) {
            xmlSuite.setThreadCount(Integer.parseInt(get(thread_count)));
        }

        if (isPresent(data_provider_thread_count)) {
            xmlSuite.setDataProviderThreadCount(Integer.parseInt(get(data_provider_thread_count)));
        }

        if (isPresent(include))
            xmlSuite.addIncludedGroup(get(include));

        if (isPresent(exclude)) {
            xmlSuite.addExcludedGroup(get(exclude));
        }

        if (isPresent(classes)) {
            String classParam = get(classes);
            List<XmlClass> classList = new ArrayList<>();
            Arrays.stream(classParam.split(","))
                    .forEach(cls -> {
                        classList.add(new XmlClass(cls));
                    });
            XmlTest test = new XmlTest();
            test.setXmlClasses(classList);
            xmlSuite.addTest(test);
        }

        if (isPresent(packages)) {
            String classParam = get(packages);
            List<XmlPackage> packageList = new ArrayList<>();
            Arrays.stream(classParam.split(","))
                    .forEach(pkg -> {
                        packageList.add(new XmlPackage(pkg));
                    });
            xmlSuite.setXmlPackages(packageList);
        }
    }
}