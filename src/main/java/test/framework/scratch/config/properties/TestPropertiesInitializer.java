package test.framework.scratch.config.properties;

import lombok.SneakyThrows;
import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.PropertySource;
import org.springframework.core.io.Resource;
import test.framework.scratch.config.EnvironmentVariables;

import static test.framework.scratch.config.EnvironmentVariables.*;

public class TestPropertiesInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @SneakyThrows
    @Override
    public void initialize(ConfigurableApplicationContext context) {
        EnvironmentVariables.setVariables(System.getenv());
        String propertyLocation = isPresent(application)
                ? "application-" + EnvironmentVariables.get(application) + ".yml"
                : "application-test.yml";
        Resource resource = context.getResource("classpath:" + propertyLocation);
        YamlPropertySourceLoader sourceLoader = new YamlPropertySourceLoader();
        PropertySource<?> yamlTestProperties = sourceLoader.load(propertyLocation, resource).get(0);

        context.getEnvironment().getPropertySources().addFirst(yamlTestProperties);
    }

}