package test.framework.scratch.config.properties.model;


import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "api")
public class RestProperties {

    @Value("${results}")
    String results;


}
