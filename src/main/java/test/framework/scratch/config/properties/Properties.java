package test.framework.scratch.config.properties;

import lombok.Data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import test.framework.scratch.config.properties.model.RestProperties;

@Data
@Configuration
@ComponentScan(basePackages = "test.framework.scratch")
@EnableConfigurationProperties(RestProperties.class)
public class Properties {

    @Value("${baseUrl}")
    private String baseUrl;


    @Value("${contentType}")
    private String contentType;

    @Autowired
    private RestProperties api;



}
