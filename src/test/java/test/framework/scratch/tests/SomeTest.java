package test.framework.scratch.tests;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import test.framework.scratch.BaseTest;
import test.framework.scratch.api.SomeTestApi;
import test.framework.scratch.config.properties.Properties;
import test.framework.scratch.dataprovider.RandomImageDataProvider;
import test.framework.scratch.steps.AssertionSteps;
import test.framework.scratch.util.FileUtil;

import static org.hamcrest.Matchers.is;

@Log4j
public class SomeTest extends BaseTest {


    @Autowired
    SomeTestApi testApi;

    @Autowired
    AssertionSteps verify;

    @Test(dataProvider = "random-image", dataProviderClass = RandomImageDataProvider.class)
    public void test(Double psi, Integer seed){
        Response response = testApi.getImage(psi, seed);

        verify
                .statusCode(response, 200)
                .that(response.getContentType(), is("image/png"));

        FileUtil.saveInputStreamFile(response.getBody().asInputStream(), "/target/images/" + psi+seed + ".png");
    }
}
