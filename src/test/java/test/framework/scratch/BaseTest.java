package test.framework.scratch;

import lombok.extern.log4j.Log4j;
import org.aspectj.lang.annotation.Before;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import test.framework.scratch.config.listeners.RequestLoggingListener;
import test.framework.scratch.config.properties.Properties;
import test.framework.scratch.config.properties.TestPropertiesInitializer;

@Log4j
//@Listeners(RequestLoggingListener.class)
@ContextConfiguration(classes = {Properties.class}, initializers = TestPropertiesInitializer.class)
public abstract class BaseTest extends AbstractTestNGSpringContextTests {


    @BeforeSuite
    public void setSomeCiVariables(){
        System.setProperty("application", "anime-girls-lol");

    }

}
