package test.framework.scratch.steps;

import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.hamcrest.Matcher;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import test.framework.scratch.exception.StepException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static test.framework.scratch.exception.StepException.Messages.*;

@Component
public class AssertionSteps {

    @Step("Assert that status code is {1}")
    public AssertionSteps statusCode(Response response, Matcher matcher) {
        assertThat(statusCodeError(response, matcher), response.getStatusCode(), matcher);
        return this;
    }
    public AssertionSteps statusCode(Response response, Integer status) {
        return status != null
                ? statusCode(response, is(status))
                : this;
    }

    public AssertionSteps equals(Object o1, Object o2) {
        return that(didntMatch(o1, o2), o1, is(equalTo(o2)));
    }

    @Step("Assert that {0}, is {1}")
    public AssertionSteps that(Object o1, Matcher matcher) {
        assertThat(didntMatch(o1, matcher), o1, matcher);
        return this;
    }

    @Step("{0}")
    public AssertionSteps that(String msg, Object o1, Matcher matcher) {
        assertThat(msg, o1, matcher);
        return this;
    }

}
