package test.framework.scratch.steps;

import com.fasterxml.jackson.core.type.TypeReference;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import test.framework.scratch.util.JsonUtil;


import static test.framework.scratch.util.FuncUtil.silentOptional;

public class BaseStep {

    @Autowired
    AssertionSteps verify;



    @Step("Mapping response to the: {1}")
    public <T> T mapIfValid(Response response, Class<T> tClass) {
        verify.statusCode(response, Matchers.is(Matchers.lessThan(300)));
        return JsonUtil.mapFromJson(response.getBody().asString(), tClass);
    }

    @Step("Verifying valid mapping to: {1}")
    public <T> Boolean isValid(Response response, Class<T> tClass) {
        return silentOptional(() -> JsonUtil.mapFromJson(response.getBody().asString(), tClass)).isPresent();
    }

    @Step()
    public <T> T mapIfValid(Response response, TypeReference<T> typeReference) {
        verify.statusCode(response, Matchers.is(Matchers.lessThan(300)));
        return JsonUtil.mapFromJson(response.getBody().asString(), typeReference);
    }
}
