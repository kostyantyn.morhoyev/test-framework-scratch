package test.framework.scratch.util;

import test.framework.scratch.exception.CheckedException;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;

public class FuncUtil {

    @FunctionalInterface
    public interface WrappedConsumer<T, R extends Exception> {
        void apply(T t) throws R;
    }
    @FunctionalInterface
    public interface WrappedPredicate<T, R extends Exception> {
        boolean apply(T t) throws R;
    }
    @FunctionalInterface
    public interface WrappedFunction<T, R, E extends Exception> {
        R apply(T t) throws E;
    }
    @FunctionalInterface
    public interface WrappedRunnable<E extends Exception> {
        void run() throws E;
    }
    @FunctionalInterface
    public interface WrappedCallable<T, E extends Exception> {
        T run() throws E;
    }
    @FunctionalInterface
    public interface WrappedFunctionOptional<T, R, E extends Exception> {
        Optional<R> apply(T t) throws E;
    }


    public static <T> Optional<T> optional(
            final WrappedCallable<T, Exception> function) {
        T returnValue = null;
        try {
            returnValue = function.run();
        } catch (Exception logged) {
            logged.printStackTrace();
            return Optional.empty();
        }
        return Optional.ofNullable(returnValue);
    }

    public static <T> Optional<T> silentOptional(
            final WrappedCallable<T, Exception> function) {
        T returnValue = null;
        try {
            returnValue = function.run();
        } catch (Exception logged) {
            return Optional.empty();
        }
        return Optional.ofNullable(returnValue);
    }

    public static <T> Consumer<T> checked(
            final WrappedConsumer<T, Exception> consumer) {
        requireNonNull(consumer);
        return (called) -> {
            try {
                consumer.apply(called);
            } catch (final Exception exception) {
                throw new RuntimeException(exception);
            }
        };
    }

    public static <T> Consumer<T> ignored(
            final WrappedConsumer<T, Exception> consumer) {
        requireNonNull(consumer);
        return (called) -> {
            try {
                consumer.apply(called);
            } catch (final Exception logged) {
                logged.printStackTrace();
            }
        };
    }

    public static void checked(
            final WrappedRunnable<Exception> runnable) {
        requireNonNull(runnable);
        try {
            runnable.run();
        } catch (final Exception e) {
            throw new CheckedException(e);
        }
    }

    public static void ignored(
            final WrappedRunnable<Exception> runnable) {
        requireNonNull(runnable);
        try {
            runnable.run();
        } catch (final Exception logged) {
            logged.printStackTrace();
        }
    }

    public static <T, R> Function<T, R> checked(
            final WrappedFunction<? super T, ? extends R, ?> function) {
        requireNonNull(function);
        return t -> {
            try {
                return function.apply(t);
            } catch (final Exception e) {
                throw new CheckedException(e);
            }
        };
    }

    public static <T, R> Function<T, R> ignored(
            final WrappedFunction<? super T, ? extends R, ?> function) {
        requireNonNull(function);
        return t -> {
            try {
                return function.apply(t);
            } catch (final Exception logged) {
                logged.printStackTrace();
                return null;
            }
        };
    }



    public static <T> Predicate<T> checked(
            final WrappedPredicate<T, Exception> predicate) {
        requireNonNull(predicate);
        return i -> {
            try {
                return predicate.apply(i);
            } catch (final Exception exception) {
                throw new CheckedException(exception);
            }
        };
    }

    public static <T> Predicate<T> ignored(
            final WrappedPredicate<T, Exception> predicate) {
        requireNonNull(predicate);
        return i -> {
            try {
                return  predicate.apply(i);
            } catch (final Exception logged) {
                logged.printStackTrace();
                return false;
            }
        };
    }



}
