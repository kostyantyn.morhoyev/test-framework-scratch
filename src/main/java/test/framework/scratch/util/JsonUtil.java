package test.framework.scratch.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import lombok.extern.log4j.Log4j;
import test.framework.scratch.exception.CheckedException;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.gson.FieldNamingPolicy.IDENTITY;

@Log4j
public class JsonUtil {

    private static GsonBuilder serializeConfig() {
        return new GsonBuilder()
                .setFieldNamingPolicy(IDENTITY)
                .disableHtmlEscaping()
                .registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
                    BigDecimal value = BigDecimal.valueOf(src);
                    return new JsonPrimitive(value.toBigInteger());
                })
                .excludeFieldsWithModifiers(Modifier.STATIC)
                .addSerializationExclusionStrategy(serializationStrategy);
    }

    public static Gson serializerWithNullValues() {
        return serializeConfig()
                .serializeNulls()
                .create();
    }

    public static Gson serializer() {
        return serializeConfig()
                .create();
    }

    public static Gson serializerWithFloatingNumbers() {
        return new GsonBuilder()
                .setFieldNamingPolicy(IDENTITY)
                .excludeFieldsWithModifiers(Modifier.STATIC)
                .addSerializationExclusionStrategy(serializationStrategy)
                .create();
    }

    public static Gson calculationSerializer() {
        return new GsonBuilder()
                .setFieldNamingPolicy(IDENTITY)
                .excludeFieldsWithModifiers(Modifier.STATIC)
                .addSerializationExclusionStrategy(serializationStrategy)
                .serializeNulls()
                .create();
    }

    private static final ExclusionStrategy serializationStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }

        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            //disabled right now.
            return false;
        }
    };

    public static <T> T mapFromJson(String json, Class<T> mapTo) {
        try {
            T returnValue = serializerWithNullValues().fromJson(json, mapTo);
            return returnValue;
        } catch (JsonSyntaxException ex) {
            log.error("Mapping error:");
            log.error(LogUtil.beautifyObjectAsJsonString(json));
            log.error(mapTo.getSimpleName());
            throw new CheckedException(json, ex);
        }
    }

    public static <T> T mapFromJson(String json, TypeReference<T> mapTo) {
        try {
            T returnValue = serializerWithNullValues().fromJson(json, mapTo.getType());
            return returnValue;
        } catch (JsonSyntaxException ex) {
            log.error("Mapping error:\n");
            log.error(LogUtil.beautifyObjectAsJsonString(json));
            log.error(mapTo.getType().getTypeName());
            throw new CheckedException(json, ex);
        }
    }

    public static <T> T mapFromJson(String json, Class<T> mapTo, Gson configs) {
        try {
            T returnValue = configs.fromJson(json, mapTo);
            return returnValue;
        } catch (JsonSyntaxException ex) {
            log.error("Mapping error:\n");
            log.error(LogUtil.beautifyObjectAsJsonString(json));
            log.error(mapTo.getSimpleName());
            throw new CheckedException(ex);
        }
    }

    public static <T> T mapFromJson(String json, TypeReference<T> mapTo, Gson configs) {
        try {
            T returnValue = configs.fromJson(json, mapTo.getType());
            return returnValue;
        } catch (JsonSyntaxException ex) {
            log.error("Mapping error:\n");
            log.error(LogUtil.beautifyObjectAsJsonString(json));
            log.error(mapTo.getType().getTypeName());
            throw new CheckedException(ex);
        }
    }

    public static <T> T mapFromLinkedMap(Object json, Class<T> mapTo) {
        try {
            T returnValue = serializerWithNullValues().fromJson(serializerWithNullValues().toJson(json), mapTo);
            return returnValue;
        } catch (JsonSyntaxException ex) {
            log.error("Mapping error:\n");
            log.error(LogUtil.beautifyObjectAsJsonString(json));
            log.error(mapTo.getSimpleName());
            throw new CheckedException(ex);
        }
    }

    public static String mapToJson(Object instance) {
        return serializerWithNullValues().toJson(instance);
    }

    public static String mapToJson(Object instance, Gson configs) {
        return configs.toJson(instance);
    }

    public static JsonElement mapToJsonTree(Object object) {
        return serializerWithNullValues().toJsonTree(object);
    }

    public static <T> List<T> mapListFromJson(String body, Class<T> entityClass) {
        TypeToken<ArrayList<T>> token = new TypeToken<ArrayList<T>>() {
        };
        return serializerWithNullValues().fromJson(body, token.getType());
    }

    public static List<Map<String, String>> jsonAsListPair(String jsonString) {
        Type listType = new TypeToken<ArrayList<HashMap<String, String>>>() {
        }.getType();
        return serializerWithNullValues().fromJson(jsonString, listType);
    }

    public static String mapToJson(Object entity, boolean serializeNulls) {
        Gson serializer = serializeNulls ? serializerWithNullValues() : serializer();
        return serializer.toJson(entity);
    }
}
