package test.framework.scratch.exception;

import io.restassured.response.Response;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;

import java.util.List;
import java.util.Map;

import static test.framework.scratch.util.LogUtil.format;

public class StepException extends RuntimeException {

    public static class Messages {
        public static String mappingError(Object where, Map replacements, Class tClass) {
            return "Can't create an instance of " + tClass + " from:"
                    + "\n" + where
                    + "\n" + replacements;
        }

        public static String mappingError(Map replacements, Object update) {
            return "Can't create an instance of " + update.getClass()
                    + " from:" + replacements;
        }

        public static String statusCodeError(Response response, List propertiesToCheck) {
            return "Assertion failed for expected properties:"
                    + "\n" + format(propertiesToCheck)
                    + "\n" + format(response.getBody().asString());
        }

        public static String statusCodeError(Response response, Matcher matcher) {
            return "Status code assertion error:"
                    + "\nStatus code: " + response.getStatusCode()
                    + "\nDidn't match: " + matcher.toString()
                    + "\nResponse:\n" + format(response.getBody().asString());
        }

        public static String didntMatch(Object o1, Matcher matcher) {
            return "Assertion failed :"
                    + "\nActual: " + format(o1)
                    + "\nDidn't match: " + format(matcher.toString());
        }

        public static String didntMatch(Object o1, Object o2) {
            return /*withLogs(*/"Assertion failed :"
                    + "\nActual: " + format(o1)
                    + "\nDidn't match: " + format(o2)/*)*/;
        }
    }

    private static String defaultErrorMessage(Object ref) {
        String assumedMethodName = (Thread.currentThread().getStackTrace()[2]).getClassName();
        assumedMethodName = StringUtils.substringAfterLast(assumedMethodName, ".");
        return "Error occurs on: \nClass: " + ref.getClass().getSimpleName() + "\nMethod: " + assumedMethodName + "\n";
    }

    private static String defaultErrorMessage() {
        String assumedMethodName = (Thread.currentThread().getStackTrace()[2]).getClassName();
        assumedMethodName = StringUtils.substringAfterLast(assumedMethodName, ".");
        return "Error occurs on: \nMethod: " + assumedMethodName + "\n";
    }

    public StepException(Object errorClassReference) {
        super(defaultErrorMessage(errorClassReference));
    }

    public StepException() {
        super(defaultErrorMessage());
    }

    public StepException(Object errorClassReference, String message) {
        super(defaultErrorMessage(errorClassReference) + message);
    }

    public StepException(String message, Throwable cause) {
        super(defaultErrorMessage() + message, cause);
    }

    public StepException(Throwable cause) {
        super(defaultErrorMessage() + cause);
    }

    protected StepException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(defaultErrorMessage() + message, cause, enableSuppression, writableStackTrace);
    }

}
