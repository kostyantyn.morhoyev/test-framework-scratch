package test.framework.scratch.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import test.framework.scratch.exception.CheckedException;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

public class FileUtil {

    public static void saveJsonToFile(Object json, String fileLocation) {
        try (Writer writer = new FileWriter(fileLocation)) {
            JsonUtil.serializer().toJson(json, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static File saveByteArrayFile(byte[] array, String fileLocation) {
        File file = new File(System.getProperty("user.dir") + fileLocation);

        try {
            file.getParentFile().mkdirs();
            file.createNewFile();

            FileInputStream fos = new FileInputStream(file);
            fos.getChannel().write(ByteBuffer.wrap(array));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

    public static File saveInputStreamFile(InputStream stream, String fileLocation) {
        File file = new File(System.getProperty("user.dir") + fileLocation);

        try {
            file.getParentFile().mkdirs();
            file.createNewFile();

            FileUtils.copyInputStreamToFile(stream, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

    public static <T> T readFromFile(String fileLocation, Class<T> mapTo) {
        InputStream resourceAsStream = FileUtil.class.getResourceAsStream(fileLocation);
        try {
            String file = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
            return JsonUtil.serializer().fromJson(file, mapTo);
        } catch (IOException e) {
            e.printStackTrace();
            throw new CheckedException(e);
        }
    }

    public static <T> T readFromFile(String fileLocation, TypeReference<T> mapTo) {
        InputStream resourceAsStream = FileUtil.class.getResourceAsStream(fileLocation);
        try {
            String file = IOUtils.toString(resourceAsStream, StandardCharsets.UTF_8);
            return JsonUtil.serializer().fromJson(file, mapTo.getType());
        } catch (IOException e) {
            e.printStackTrace();
            throw new CheckedException(e);
        }
    }

    public static <T> T readAndReplace(String fileLocation, Object replacementJson, Class<T> mapTo) {
        InputStream resourceAsStream = FileUtil.class.getResourceAsStream(fileLocation);
        JsonElement base = JsonUtil.serializer().toJsonTree(resourceAsStream);
        JsonElement replacement = JsonUtil.serializer().toJsonTree(replacementJson);
        JsonArray result = base.getAsJsonArray();
        result.addAll(replacement.getAsJsonArray());
        return JsonUtil.serializer().fromJson(result.getAsString(), mapTo);
    }

    public static <T> T readAndReplace(String fileLocation, Object replacementJson, TypeReference<T> mapTo) {
        InputStream resourceAsStream = FileUtil.class.getResourceAsStream(fileLocation);
        JsonElement base = JsonUtil.serializer().toJsonTree(resourceAsStream);
        JsonElement replacement = JsonUtil.serializer().toJsonTree(replacementJson);
        JsonArray result = base.getAsJsonArray();
        result.addAll(replacement.getAsJsonArray());
        return JsonUtil.serializer().fromJson(result.getAsString(), mapTo.getType());
    }
}
