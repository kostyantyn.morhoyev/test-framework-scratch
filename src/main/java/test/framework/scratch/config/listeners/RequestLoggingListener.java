package test.framework.scratch.config.listeners;

import io.restassured.RestAssured;
import io.restassured.filter.Filter;
import io.restassured.filter.FilterContext;
import io.restassured.response.Response;
import io.restassured.specification.FilterableRequestSpecification;
import io.restassured.specification.FilterableResponseSpecification;
import jdk.javadoc.doclet.Reporter;
import lombok.extern.log4j.Log4j;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static test.framework.scratch.util.LogUtil.beautifyBody;
import static test.framework.scratch.util.LogUtil.getOrReplaceWith;

@Log4j
public class RequestLoggingListener implements ITestListener {

    @Override
    public void onStart(ITestContext context) {
        RestAssured.filters(
                new RequestLoggingFilter()
                // new FailedRequestFilter()
        );
    }


    private String getRequestData(FilterableRequestSpecification requestSpec, Response response) {
        String defaultEmptyString = "no value present";
        StringBuilder requestBuilder = new StringBuilder();
        StringBuilder responseBuilder = new StringBuilder();
        // request log
        requestBuilder.append("\nREQUEST: ");
        requestBuilder.append(requestSpec.getURI()).append(" --> ").append(requestSpec.getMethod());
        requestBuilder.append("\nRequest headers:\n");
        requestBuilder.append(getOrReplaceWith(requestSpec.getHeaders().toString(), defaultEmptyString));
        requestBuilder.append("\n");
        if (Objects.nonNull(requestSpec.getBody())) {
            requestBuilder.append("Request Body:\n")
                    .append(beautifyBody(requestSpec.getContentType(), requestSpec.getBody().toString()));
        }
        // response log
        responseBuilder.append("\nRESPONSE: ");
        responseBuilder.append(getOrReplaceWith(response.statusLine(), defaultEmptyString));
        responseBuilder.append("--> ").append(response.getTimeIn(TimeUnit.SECONDS)).append(" sec");
        responseBuilder.append("\nResponse headers:\n");
        responseBuilder.append(getOrReplaceWith(response.getHeaders().toString(), defaultEmptyString));
        responseBuilder.append("\nResponse Body:\n");
        responseBuilder.append(beautifyBody(response.getContentType(), response.getBody().asString())).append("\n");
        return requestBuilder.append(responseBuilder).toString();
    }

    // log all requests and responses
    private class RequestLoggingFilter implements Filter {
        @Override
        public Response filter(FilterableRequestSpecification requestSpecification,
                               FilterableResponseSpecification responseSpecification, FilterContext filterContext) {
            Response response = filterContext.next(requestSpecification, responseSpecification);
            log.info(getRequestData(requestSpecification, response));
            return response;
        }
    }

    // log only for 400+ requests and responses
    public class FailedRequestFilter implements Filter {
        @Override
        public Response filter(FilterableRequestSpecification requestSpecification,
                               FilterableResponseSpecification responseSpecification, FilterContext filterContext) {
            Response response = filterContext.next(requestSpecification, responseSpecification);
            if (response.statusCode() >= 400) {
                log.info(getRequestData(requestSpecification, response));
            }
            return response;
        }
    }
}